
CREATE TABLE product_tb
(
    product_id serial PRIMARY KEY NOT NULL,
    product_name varchar(50) NOT NULL,
    product_price float NOT NULL
);

CREATE TABLE customer_tb
(
    customer_id serial PRIMARY KEY NOT NULL,
    customer_name varchar(50) NOT NULL,
    customer_address varchar NOT NULL,
    customer_phone varchar(50) NOT NULL
);

CREATE TABLE invoice_tb
(
    invoice_id serial PRIMARY KEY NOT NULL,
    invoice_date varchar NOT NULL,
    customer_id int NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customer_tb(customer_id)
        ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE invoice_detail_tb
(
    id serial PRIMARY KEY NOT NULL,
    invoice_id int NOT NULL,
    product_id int NOT NULL,
    FOREIGN KEY (product_id) REFERENCES product_tb(product_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (invoice_id) REFERENCES invoice_tb(invoice_id)
        ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
VALUES ('bobo', 'pp', '093 343 343');

UPDATE customer_tb
SET customer_name = 'sovita', customer_address = 'phnom penh', customer_phone = '000000'
WHERE customer_id = 2 returning *;

DELETE FROM customer_tb WHERE customer_id = 5;


SELECT p.product_id, p.product_name, p.product_price FROM product_tb p
INNER JOIN invoice_detail_tb idt on p.product_id = idt.product_id
WHERE invoice_id = 4;