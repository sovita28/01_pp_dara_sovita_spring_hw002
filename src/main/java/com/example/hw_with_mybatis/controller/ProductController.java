package com.example.hw_with_mybatis.controller;

import com.example.hw_with_mybatis.model.entity.Product;
import com.example.hw_with_mybatis.model.reponse.ReponseAPI;
import com.example.hw_with_mybatis.service.IProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final IProductService productService;

    public ProductController(IProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/getAllProduct")
    public ResponseEntity<?> getAllProduct(){
        return ResponseEntity.ok(new ReponseAPI<>(
                productService.getAllProduct(),
                "Successfully.",
                true
        ));
    }

    @GetMapping("/getProductById/{id}")
    public ResponseEntity<?> getProductById(@PathVariable int id){
        return ResponseEntity.ok(new ReponseAPI<>(
                productService.getProductById(id),
                "Successfully.",
                true
        ));
    }

    @PostMapping("/addProduct")
    public ResponseEntity<?> insertProduct(@RequestBody Product product){
        return ResponseEntity.ok(new ReponseAPI<>(
                productService.insertProduct(product),
                "Added Successfully.",
                true
        ));
    }


    @DeleteMapping("/deleteProductById/{id}")
    public ResponseEntity<?> deleteProductById(@PathVariable int id){
        return ResponseEntity.ok(new ReponseAPI<>(
                productService.deleteProductById(id),
                "Deleted Successfully.",
                true
        ));
    }


    @PutMapping("/updateProductById/{id}")
    public ResponseEntity<?> updateProductById(@PathVariable int id, @RequestBody Product product){
        return ResponseEntity.ok(new ReponseAPI<>(
                productService.updateProductById(id, product),
                "Updated Successfully.",
                true
        ));
    }
}
