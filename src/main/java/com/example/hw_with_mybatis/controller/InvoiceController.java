package com.example.hw_with_mybatis.controller;

import com.example.hw_with_mybatis.model.Request.InvoiceRequest;
import com.example.hw_with_mybatis.model.entity.Customer;
import com.example.hw_with_mybatis.model.entity.Product;
import com.example.hw_with_mybatis.model.reponse.ReponseAPI;
import com.example.hw_with_mybatis.service.IInvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {
    private final IInvoiceService invoiceService;

    public InvoiceController(IInvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping("/getAllInvoice")
    public ResponseEntity<?> getAllInvoice(){
        return ResponseEntity.ok(new ReponseAPI<>(
                invoiceService.getAllInvoice(),
                "Successfully.",
                true
        ));
    }

    @GetMapping("/getInvoiceById/{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable int id){
        return ResponseEntity.ok(new ReponseAPI<>(
                invoiceService.getInvoiceById(id),
                "Successfully.",
                true
        ));
    }
    @PostMapping("/insertInvoice")
    public ResponseEntity<?> insertInvoice(@RequestBody InvoiceRequest invoiceRequest){
        Integer invId = invoiceService.insertInvoice(invoiceRequest);
        if(invId != null){
            return ResponseEntity.ok(new ReponseAPI<>(
                    invoiceService.getInvoiceById(invId),
                    "Added Successfully.",
                    true
            ));
        }
        return null;
    }

    @PutMapping("/updateInvoice/{id}")
    public ResponseEntity<?> updateInvoiceById(@PathVariable int id, @RequestBody InvoiceRequest invoiceRequest){
        return ResponseEntity.ok(new ReponseAPI<>(
                invoiceService.updateInvoiceById(id, invoiceRequest),
                "Updated Successfully.",
                true
        ));
    }

    @DeleteMapping("/deleteInvoice/{id}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable int id){
        invoiceService.deleteInvoiceById(id);
        return ResponseEntity.ok(new ReponseAPI<>(
                null,
                "Deleted Successfully.",
                true
        ));
    }

}
