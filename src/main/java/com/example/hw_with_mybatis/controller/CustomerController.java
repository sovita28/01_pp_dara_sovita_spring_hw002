package com.example.hw_with_mybatis.controller;

import com.example.hw_with_mybatis.model.entity.Customer;
import com.example.hw_with_mybatis.model.reponse.ReponseAPI;
import com.example.hw_with_mybatis.service.ICustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private final ICustomerService customerService;

    public CustomerController(ICustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/getAllCustomer")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(new ReponseAPI<>(
                customerService.getAllCustomer(),
                "Successfully.",
                true
        ));
    }
    @GetMapping("/getCustomerById/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable int id){
        return ResponseEntity.ok(new ReponseAPI<>(
                customerService.getCustomerById(id),
                "Successfully.",
                true
        ));
    }
    @PostMapping("/addCustomer")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer customer){
        return ResponseEntity.ok(new ReponseAPI<>(
                customerService.insertCustomer(customer),
                "Successfully added.",
                true
        ));
    }

    @PutMapping("/updateCustomerById/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable int id,@RequestBody Customer customer){
        return ResponseEntity.ok(new ReponseAPI<>(
                customerService.updateCustomerById(id, customer),
                "Updated Successfully.",
                true
        ));
    }

    @DeleteMapping("/deleteCustomerById/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable int id){
        return ResponseEntity.ok(new ReponseAPI<>(
                customerService.deleteCustomerById(id),
                "Deleted Successfully.",
                true
        ));
    }
}
