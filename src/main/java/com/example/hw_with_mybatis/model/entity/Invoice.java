package com.example.hw_with_mybatis.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Invoice {
    private int invoiceId;
    private String invoiceDate;
    private Customer customerId;
    private List<Product> products = new ArrayList<>();
}
