package com.example.hw_with_mybatis.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Product {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int productId;
    private String productName;
    private double productPrice;
}
