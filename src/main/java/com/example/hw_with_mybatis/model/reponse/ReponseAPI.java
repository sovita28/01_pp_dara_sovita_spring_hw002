package com.example.hw_with_mybatis.model.reponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ReponseAPI <T> {
    private T payload;
    private String message;
    private boolean success = false;
}
