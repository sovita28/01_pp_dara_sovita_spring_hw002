package com.example.hw_with_mybatis.model.Request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequest {
    private String invoiceDate;
    private Integer customerId;
    private List<Integer> productId;
}
