package com.example.hw_with_mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HwWithMyBatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(HwWithMyBatisApplication.class, args);
    }

}
