package com.example.hw_with_mybatis.service;

import com.example.hw_with_mybatis.model.entity.Product;

import java.util.List;

public interface IProductService {

    List<Product> getAllProduct();

    Product getProductById(int id);

    Product insertProduct(Product product);

    Product deleteProductById(int id);

    Product updateProductById(int id, Product product);
}
