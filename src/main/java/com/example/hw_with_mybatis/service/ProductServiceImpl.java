package com.example.hw_with_mybatis.service;

import com.example.hw_with_mybatis.model.entity.Product;
import com.example.hw_with_mybatis.repository.IProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements IProductService{

    private final IProductRepository productRepository;

    public ProductServiceImpl(IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(int id) {
        return productRepository.getAllProductById(id);
    }

    @Override
    public Product insertProduct(Product product) {
        return productRepository.insertProduct(product);
    }

    @Override
    public Product deleteProductById(int id) {
        return productRepository.deleteProductById(id);
    }

    @Override
    public Product updateProductById(int id, Product product){
        return productRepository.updateProductById(id, product);
    }
}
