package com.example.hw_with_mybatis.service;

import com.example.hw_with_mybatis.model.entity.Customer;

import java.util.List;

public interface ICustomerService {

    List<Customer> getAllCustomer();

    Customer insertCustomer(Customer customer);

    Object getCustomerById(int id);

    Object updateCustomerById(int id, Customer customer);

    Object deleteCustomerById(int id);
}
