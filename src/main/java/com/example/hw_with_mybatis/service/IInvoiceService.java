package com.example.hw_with_mybatis.service;

import com.example.hw_with_mybatis.model.Request.InvoiceRequest;
import com.example.hw_with_mybatis.model.entity.Customer;
import com.example.hw_with_mybatis.model.entity.Invoice;
import com.example.hw_with_mybatis.model.entity.Product;

import java.util.List;

public interface IInvoiceService {
    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(int id);

    Invoice updateInvoiceById(int id, InvoiceRequest invoiceRequest);

    Integer insertInvoice(InvoiceRequest invoiceRequest);

    void deleteInvoiceById(int id);
}
