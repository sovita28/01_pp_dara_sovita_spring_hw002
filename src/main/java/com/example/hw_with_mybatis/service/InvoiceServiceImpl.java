package com.example.hw_with_mybatis.service;

import com.example.hw_with_mybatis.model.Request.InvoiceRequest;
import com.example.hw_with_mybatis.model.entity.Customer;
import com.example.hw_with_mybatis.model.entity.Invoice;
import com.example.hw_with_mybatis.model.entity.Product;
import com.example.hw_with_mybatis.repository.IInvoiceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl implements IInvoiceService{

    private final IInvoiceRepository invoiceRepository;

    public InvoiceServiceImpl(IInvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(int id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public Invoice updateInvoiceById(int id, InvoiceRequest invoiceRequest) {

        Invoice invoice = invoiceRepository.updateInvoiceById(id, invoiceRequest);
        for(Integer proId : invoiceRequest.getProductId()) {
            invoiceRepository.updateIntoInvoiceDetail(id, proId);
        }
        return invoice;
    }

    @Override
    public Integer insertInvoice(InvoiceRequest invoiceRequest) {

        Invoice invoice = invoiceRepository.insertInvoice(invoiceRequest);
        for(Integer proId : invoiceRequest.getProductId()) {
            invoiceRepository.insertIntoInvoiceDetail(invoice.getInvoiceId(), proId);
        }
        return invoice.getInvoiceId();
    }

    @Override
    public void deleteInvoiceById(int id) {
        invoiceRepository.deleteInvoiceById(id);
    }
}
