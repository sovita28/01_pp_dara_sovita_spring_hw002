package com.example.hw_with_mybatis.service;

import com.example.hw_with_mybatis.model.entity.Customer;
import com.example.hw_with_mybatis.repository.ICustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements ICustomerService{

    private final ICustomerRepository customerRepository;

    public CustomerServiceImpl(ICustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer insertCustomer(Customer customer) {
        return customerRepository.insertCustomer(customer);
    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer updateCustomerById(int id, Customer customer) {
        return customerRepository.updateCustomerById(id, customer);
    }

    @Override
    public Customer deleteCustomerById(int id) {
        return customerRepository.deleteCustomerById(id);
    }
}
