package com.example.hw_with_mybatis.repository;

import com.example.hw_with_mybatis.model.entity.Invoice;
import com.example.hw_with_mybatis.model.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface IInvoiceDetailRepository {
    @Select("""
            SELECT p.product_id, p.product_name, p.product_price FROM product_tb p
            INNER JOIN invoice_detail_tb idt on p.product_id = idt.product_id
            WHERE invoice_id = #{id}
            """)
    @Results(id = "proMap", value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price")
    })
    public List<Product> getAllByInvoiceId(@Param("id") int invoice_Id);
}
