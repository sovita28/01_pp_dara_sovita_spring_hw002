package com.example.hw_with_mybatis.repository;

import com.example.hw_with_mybatis.model.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface IProductRepository {

    @Select("""
            SELECT * FROM product_tb
            """)
    @Results(id = "proMap", value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price"),
    })
    List<Product> getAllProduct();


    @Select("""
            SELECT * FROM product_tb WHERE product_id = #{id}
            """)
    @ResultMap("proMap")
    Product getAllProductById(int id);


    @Select("""
            INSERT INTO product_tb (product_name, product_price)
            VALUES (#{pro.productName}, #{pro.productPrice})
            RETURNING *
            """)
    @ResultMap("proMap")
    Product insertProduct(@Param("pro") Product product);

    @Select("""
            DELETE FROM product_tb WHERE product_id = #{id}
            RETURNING *
            """)
    @ResultMap("proMap")
    Product deleteProductById(int id);


    @Select("""
            UPDATE product_tb
            SET product_name = #{pro.productName}, product_price = #{pro.productPrice}
            WHERE product_id = #{id}
            RETURNING *
            """)
    @ResultMap("proMap")
    Product updateProductById(int id,@Param("pro") Product product);
}
