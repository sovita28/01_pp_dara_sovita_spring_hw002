package com.example.hw_with_mybatis.repository;

import com.example.hw_with_mybatis.model.Request.InvoiceRequest;
import com.example.hw_with_mybatis.model.entity.Customer;
import com.example.hw_with_mybatis.model.entity.Invoice;
import com.example.hw_with_mybatis.model.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface IInvoiceRepository {

    //    get all invoice
    @Select("""
            SELECT * FROM invoice_tb
            """)
    @Results(id = "invMap", value = {
            @Result(property = "invoiceId", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customerId", column = "customer_id",
                    one = @One(select = "com.example.hw_with_mybatis.repository.ICustomerRepository.getCustomerById")
            ),
            @Result(property = "products", column = "invoice_id",
                    many = @Many(select = "com.example.hw_with_mybatis.repository.IInvoiceDetailRepository.getAllByInvoiceId")
            )
    })
    List<Invoice> getAllInvoice();

    //    get invoice by ID
    @Select("""
            SELECT * FROM invoice_tb
            WHERE invoice_id = #{id}
            """)
    @ResultMap("invMap")
    Invoice getInvoiceById(@Param("id") int id);

//    -----update invoice
    @Select("""
            UPDATE invoice_tb SET
            invoice_date = #{invoice.invoiceDate}, customer_id = #{invoice.customerId}
            WHERE invoice_id = ${id}
            RETURNING *
            """)
    @ResultMap("invMap")
    Invoice updateInvoiceById(int id,@Param("invoice") InvoiceRequest invoiceRequest);


    @Update("""
            UPDATE invoice_detail_tb
            SET product_id = #{productId}
            WHERE invoice_id = #{invoiceId};
            """)
    @ResultMap("invMap")
    void updateIntoInvoiceDetail(Integer invoiceId, Integer productId);

//    -------insert invoice
    @Select("""
            INSERT INTO invoice_tb (invoice_date, customer_id)
            VALUES (#{invoice.invoiceDate}, #{invoice.customerId})
            RETURNING *
            """)
    @ResultMap("invMap")
    Invoice insertInvoice(@Param("invoice") InvoiceRequest invoiceRequest);


    @Insert("""
            INSERT INTO invoice_detail_tb (invoice_id, product_id)
            VALUES (#{invoiceId}, #{productId})
            """)
    @ResultMap("invMap")
    void insertIntoInvoiceDetail(Integer invoiceId, Integer productId);

//    ------delete by ID

    @Select("""
            DELETE FROM invoice_tb
            WHERE invoice_id = #{id}
            """)
    void deleteInvoiceById(int id);
}
