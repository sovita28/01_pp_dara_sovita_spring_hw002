package com.example.hw_with_mybatis.repository;

import com.example.hw_with_mybatis.model.entity.Customer;
//import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ICustomerRepository {

    @Select("""
            SELECT * FROM customer_tb
            """)
    @Results(id = "cusMap", value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAddress", column = "customer_address"),
            @Result(property = "customerPhone", column = "customer_phone")
    })
    List<Customer> getAllCustomer();

    @Select("""
            INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
            VALUES (#{request.customerName}, #{request.customerAddress}, #{request.customerPhone})
            RETURNING *
            """)
    @ResultMap("cusMap")
    Customer insertCustomer(@Param("request") Customer customer);

    @Select("""
            SELECT * FROM customer_tb where customer_id = #{id}
            """)
    @ResultMap("cusMap")
    Customer getCustomerById(int id);



    @Select("""
            UPDATE customer_tb
            SET customer_name = #{cus.customerName}, customer_address = #{cus.customerAddress}, customer_phone = #{cus.customerPhone}
            WHERE customer_id = #{id} RETURNING *
            """)
    @ResultMap("cusMap")
    Customer updateCustomerById(int id,@Param("cus")  Customer customer);


    @Select("""
            DELETE FROM customer_tb WHERE customer_id = #{id}
            RETURNING *;
            """)
    @ResultMap("cusMap")
    Customer deleteCustomerById(int id);
}
